<?php ob_start(); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include "../includes/headers2.txt";include("includes/head.php"); ?>
<?php $place = "popcompub"; include("script/suggest.php"); //autocomplete--suggest?>
</head>
<body>
<div id="wrapper1">
<div id="fixed-header">
		<?php include '../includes/headerstyle2.txt'; ?>
			<div id="contents">
				<div id="page-content">
						<div id="dock-icons">
							<?php include '../includes/dockicons2.php'; //icons from the dockbar?>
						</div>
						<div id="contents-holder">							
							<div class="panel-holder">
								<div id="left">
									<?php include("includes/leftcontent.php"); ?>
								</div>
								<div id="right" class="globalroundedcorners">
									<div id="upper-search" class="globalroundedcorners">
										<?php function popub(){ ?>
										<span class="title">POPCOM Publications</span><br /><br />
										<form action="" method="get">
										<select name="category" class="roundedcorners">
										<option value="title">Title</option>
										<option value="author">Author</option>
										<option value="subject">Subject</option>
										<option value="call_no">Call Number</option>
										<option value="ISBN_ISSN">ISBN/ISSN</option>
										</select>
										<input type="text" name="searchpop" id="searchpop" size="25" class="roundedcorners"/>&nbsp;&nbsp;
										<input type="submit" name="doSearch" id="defaultButton" value="Search" class="button orange bigrounded"/>
										</form>										
									</div>
									<div id="pub-contents" class="globalroundedcorners">
										<?php } ?>
										<?php
										require_once("includes/removeslash.php");//for encryption and decryption
										require_once("includes/Encryption.php");//for encryption and decryption
										$encrypt_value = new Encryption();
										require_once("class/clean.php");//for data cleaning
										$clean = new Clean();
										require_once("class/pagination.php");//for pagination
										$paginations = new paginations();
										require_once("class/formelement.php");//for form element
										$formelement = new formelement();
										$secret_code = "imadreamer";
										if(isset($_GET['doSearch']))
										{
											echo popub();
											$category = $_GET['category'];
											$searchpop = $_GET['searchpop'];
											$searchpop = trim($searchpop);
											$searchpop = str_replace("%","",$searchpop);
											$doSearch = $_GET['doSearch'];
											if(empty($searchpop))
											{
												echo "<center><b>No Result Found</b></center>";
												echo "<table id='pub-table' width='570px'><tr><td class='t_head'>Format</td><td class='t_head'>Title</td></tr>";
												echo "</table>";
											}
											else
											{	
												$searchpop = $clean->RemoveDirt($searchpop);
												$searchpop = $category." LIKE \"%".$searchpop."%\"".$secret_code;
												$encrypted =$encrypt_value->encode($searchpop);
												header("Location: popcom_pub.php?search=".$encrypted."");
											}
										}
										else if(isset($_GET['search']))
										{
											echo popub();
											$search = $_GET['search'];
											$decode = $encrypt_value->decode($search);//decode encoded search value
											$decoded_search = str_replace($secret_code,"",$decode);//remove secret code
											$tmprng = $decoded_search.$secret_code;
											if($encrypt_value->encode($tmprng) != $search){echo "Looking for something??it might not be here";}
											else{
												$connect ="../includes/connect_db.php";
												$table = "materials WHERE $decoded_search AND publisher LIKE '%popcom%'";//table or table with where clause
												$adjacents=3;
												$targetpage="popcom_pub.php";//target page
												
												$limit = $clean->RemoveDirt(@$_GET['limit']);if(empty($limit) || $limit <= 0 || !ctype_digit($limit)){$limit = 10;}else{$limit = $clean->RemoveMagic($limit);}//limit to show in
												$page = @$_GET['page']; //get page number
												$addtourl = "search=".$search."&limit=".$limit;
												$get_pgntn = $paginations->pagination_query($connect,$table,$adjacents,$targetpage,$limit,$page,$addtourl);
												if($page < 2){$pagenum = 1;}else{$pagenum = (($limit*$page)-1);}//for paging on brief results
												require_once("class/get_string_between.php");
												echo "<center>".$paginations->TotalResult()."<b> Results Found for".@$_GET['limit'].": </b>".stripslashes(get_string_between($decoded_search,"%","%"))."</center>";
												echo "<table width='570px' id='pub-table'><tr><td colspan=\"2\">";
												echo $paginations->show_pagination_pn()."</td><td></td><td><a href=\"javascript:void(0)\" onclick=\"window.open('includes/print_results.php?print=".$encrypt_value->encode($table.$secret_code)."&fm=3','mywindow','width=550,height=670,scrollbars=yes')\" title=\"Print Results\" style='margin-left:20px;'><img src=\"images/print.gif\" width=\"20\" height=\"20\" ></a></td></tr>";
												echo "<tr><td class='t_head'>Format</td><td class='t_head'>Titles</td><td class='t_head'>Location</td><td class='t_head'>Call No.</td></tr>";
												while($query_row = mysql_fetch_array($get_pgntn))
												{
													echo "<tr><td class=\"t_result\">".$query_row['format']."</td><td class=\"t_result\"><a href=\"popcom_pubresult.php?$addtourl&page=$pagenum\">".$query_row['title']."</a></td><td class=\"t_result\">".$query_row['location']."</td></td><td class=\"t_result\">".$query_row['call_no']."</td></tr>";
													$pagenum++;
												}
												echo "<tr><td>";
												echo $paginations->show_pagination()."</td>";
												$option = array(2,"2",5,"5",10,"10");
												echo "<td colspan=3 align=\"right\"><form action=\"\" method=\"get\">";
												echo "<input type=\"hidden\" name=\"search\" value=\"$search\">";
												echo " Show Records per page".$formelement->OptionSubmit($option,"limit",$limit);
												echo "</form></td></tr>";
												echo "</table>";
											}
										}
										else
										{
											echo popub();
											$connect ="../includes/connect_db.php";
											$table = "materials WHERE publisher LIKE '%popcom%'";//table or table with where clause
											$adjacents=3;
											$targetpage="popcom_pub.php";//target page
											if(isset($_GET['limit'])){$_GET['limit']=$_GET['limit'];}else{$_GET['limit']="";}
											$limit = $_GET['limit'];if(empty($limit) || $limit <= 0 || !ctype_digit($limit)){$limit = 10;}else{$limit = $clean->RemoveMagic($limit);}//limit to show in
											if(isset($_GET['page'])){$page = $_GET['page'];}else{$page="";} //get page number
											$addtourl = "limit=$limit";
											$get_pgntn = $paginations->pagination_query($connect,$table,$adjacents,$targetpage,$limit,$page,$addtourl);
											if($page < 2){$pagenum = 1;}else{$pagenum = (($limit*$page)-1);}//for paging on brief results
											
											echo "<table id='pub-table' width='570px'><tr><td colspan=\"2\">";
											echo $paginations->show_pagination_pn()."</td><td></td><td><a href=\"javascript:void(0)\" onclick=\"window.open('includes/print_results.php?print=".$encrypt_value->encode($table.$secret_code)."&fm=3','mywindow','width=550,height=670,scrollbars=yes')\" title=\"Print Results\" style='margin-left:20px;'><img src=\"images/print.gif\" width=\"20\" height=\"20\" ></a></td></tr>";
											echo "<tr><td class='t_head'>Format</td><td class='t_head'>Title</td><td class='t_head'>Location</td><td class='t_head'>Call No.</td></tr>";
											while($query_row = mysql_fetch_array($get_pgntn))
											{
												echo "<tr><td class=\"t_result\">".$query_row['format']."</td><td class=\"t_result\"><a href=\"popcom_pubresult.php?page=$pagenum\">".$query_row['title']."</a></td><td class=\"t_result\">".$query_row['location']."</td><td class=\"t_result\">".$query_row['call_no']."</td></tr>";
												$pagenum++;
											}
											echo "<tr><td>";
											echo $paginations->show_pagination()."</td>";
											$option = array(2,"2",5,"5",10,"10");
											echo "<td colspan=3 align=\"right\"><form action=\"\" method=\"get\">";
											echo " Records per page".$formelement->OptionSubmit($option,"limit",$limit);
											echo "</form></td></tr>";
											echo "</table>";
										}
										?>
									</div>
								</div>
							</div>							
						</div>
			   </div>        
			</div>
			<div id="footer">
				<?php include '../includes/footer_content.php' ?>		
				</div>
		</div>
	</div>
</div>
</body>
</html>
<?php ob_flush(); ?>