<?php ob_start(); ?>
<?php session_start(); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include "../includes/headers2.txt";include("includes/head.php"); ?>
</head>
<body>
<div id="wrapper1">
	<div id="fixed-header">
		<?php include '../includes/headerstyle2.txt'; ?>
			<div id="contents">
				<div id="page-content">
						<div id="dock-icons">
							<?php include '../includes/dockicons2.php'; //icons from the dockbar?>
						</div>
						<div id="contents-holder">							
							<div class="panel-holder" >
								<div id="left">
									<?php include("includes/leftcontent.php"); ?>
								</div>
								<div id="right" class="globalroundedcorners">
									<span class="title" id="search-title">New Aquisitions</span><br /><br />
										<?php
										echo "<table width='570px' id='pub-table' style='margin-left:50px;'>";
										echo "<tr><td></td><td></td><td></td><td align=right><img src=\"images/print.gif\" width=\"20\" height=\"20\" ></td></tr>";
										echo "<tr><td class='t_head'>Title</td><td class='t_head'>Abstract</td><td class='t_head'>Publisher</td><td class='t_head'>Date of Publication</td>";
										echo "</table>";?>
								</div>
							</div>							
						</div>
			   </div>        
			</div>
			<div id="footer">
				<?php include '../includes/footer_content.php' ?>		
				</div>
		</div>
	</div>
</div>
</body>
</html>
<?php ob_flush(); ?>