<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include "../includes/headers2.txt";include("includes/head.php"); ?>
<script type="text/javascript">
/*js for the tab*/
$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
</script>
</head>
<body>
<div id="wrapper1">
<div id="fixed-header">
		<?php include '../includes/headerstyle2.txt'; ?>
			<div id="contents">
				<div id="page-content">
						<div id="dock-icons">
							<?php include '../includes/dockicons2.php'; //icons from the dockbar?>
						</div>
						<div id="contents-holder">							
							<div class="panel-holder" >
								<br/><br/><br/><br/>
								<div id="left">
									<?php include("includes/leftcontent.php"); ?>
								</div>
								<div id="right" class="globalroundedcorners">
									<?php
									 include("../includes/connect_db.php");
									?>
									<ul class="tabs">
									<li><a href="#briefrecord">Brief Record</a></li>
									<li><a href="#fullrecord">Full Record</a></li>
									&nbsp;&nbsp;&nbsp;<a href="popcom_pub.php<?php if(!empty($_GET['search'])){echo "?search=".$_GET['search'];}else{} ?>">Back to Search Result</a>
									</ul>

									<div class="tab_container">
										<?php include("../includes/connect_db.php");
										require_once("includes/Encryption.php");//for encryption and decryption
										$encrypt_value = new Encryption();
										require_once("class/pagination.php");//for pagination
										$paginations = new paginations();
										if(isset($_GET['search'])&&isset($_GET['page'])){$search = $_GET['search'];$load = $_GET['page'];}
										else{$search="";$load="";}
										if(!empty($search))
										{
											$secret_code = "imadreamer";
											$dencrypt_query = new Encryption();
											$decrypted_query = $dencrypt_query->decode($search);//decode encoded query
											$query_orig = str_replace($secret_code,"",$decrypted_query);//remove secret code
											$tampered_check = $query_orig.$secret_code;
											$tampered_check =$dencrypt_query->encode($tampered_check);//will be used to check tampering on url
											if($tampered_check != $search){//check if there is tampering
												echo "TAMPERING EXIST!!!!<br />";
											}
											else{
												$connect ="../includes/connect_db.php";
												$table = "materials WHERE $query_orig AND publisher LIKE '%popcom%'";//table or table with where clause
												$adjacents=3;
												$targetpage="popcom_pubresult.php";//target page
												$limit = 1;//limit to show in
												$page = $_GET['page']; //get page number
												$addtourl = "search=".$search;
												$get_pgntn = $paginations->pagination_query($connect,$table,$adjacents,$targetpage,$limit,$page,$addtourl);
												echo $paginations->show_pagination_pn();
												echo "<div id=\"briefrecord\" class=\"tab_content\"><table width='670px'>";
												$query_row =@ mysql_fetch_array($get_pgntn);
												//brief description
													echo "<tr><td class='left_td'>Tilte :</td><td class='right_td'>".$query_row['title']."</td><td rowspan=\"7\"><img src=\"./e_lib_materials/cover/".$query_row['cover']."\"></td></tr>";
													echo "<tr><td class='left_td'>Subject :</td><td class='right_td'>".$query_row['subject']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Physical Description :</td><td class='right_td'>".$query_row['physical_description']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Location :</td><td class='right_td'>".$query_row['location']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Status :</td><td class='right_td'>".$query_row['status']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Format :</td><td class='right_td'>".$query_row['format']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Download :</td><td class='right_td'><a href=\"\" onclick=\"window.open('./class/force_dl.php?elif=".$query_row['file']."','download','width=400,height=200,left=0,top=100,screenX=0,screenY=100');\">".$query_row['file']."</a></td><td></td></tr>";
													
												
												echo "</table></div>";
												echo "<div id=\"fullrecord\" class=\"tab_content\"><table width='670px'>";
												//full description
														echo "<tr><td class='left_td'>Tilte :</td><td class='right_td'>".$query_row['title']."</td><td rowspan=\"14\"><img src=\"./e_lib_materials/cover/".$query_row['cover']."\"></td></tr>";
														echo "<tr><td class='left_td'>ISBN/ISSN :</td><td class='right_td'>".$query_row['ISBN_ISSN']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Subject :</td><td class='right_td'>".$query_row['subject']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Author :</td><td class='right_td'>".$query_row['author']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Call Number :</td><td class='right_td'>".$query_row['call_no']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Publisher :</td><td class='right_td'>".$query_row['publisher']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Number of Copies :</td><td class='right_td'>".$query_row['copies']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Physical Description :</td><td class='right_td'>".$query_row['physical_description']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Location :</td><td class='right_td'>".$query_row['location']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Status :</td><td class='right_td'>".$query_row['status']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Format :</td><td class='right_td'>".$query_row['format']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Series Statement :</td><td class='right_td'>".$query_row['series_statement']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>General Note :</td><td class='right_td'>".$query_row['general_note']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Download :</td><td class='right_td'><a href=\"\" onclick=\"window.open('./class/force_dl.php?elif=".$query_row['file']."','download','width=400,height=200,left=0,top=100,screenX=0,screenY=100');\">".$query_row['file']."</a></td><td></td></tr>";
														
												
												echo "</table></div>";
											}
										}
										else
										{
											$connect ="../includes/connect_db.php";
												$table = "materials WHERE publisher LIKE '%popcom%'";//table or table with where clause
												$adjacents=3;
												$targetpage="popcom_pubresult.php";//target page
												$limit = 1;//limit to show in
												$page = @$_GET['page']; //get page number
												$addtourl = "search=".$search;
												$get_pgntn = $paginations->pagination_query($connect,$table,$adjacents,$targetpage,$limit,$page,$addtourl);
												echo $paginations->show_pagination_pn();
												echo "<div id=\"briefrecord\" class=\"tab_content\"><table width='670px'>";
												$query_row =@ mysql_fetch_array($get_pgntn);
												//brief description
													echo "<tr><td class='left_td'>Tilte :</td><td class='right_td'>".$query_row['title']."</td><td rowspan=\"7\"><img src=\"./e_lib_materials/cover/".$query_row['cover']."\"></td></tr>";
													echo "<tr><td class='left_td'>Subject :</td><td class='right_td'>".$query_row['subject']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Physical Description :</td><td class='right_td'>".$query_row['physical_description']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Location :</td><td class='right_td'>".$query_row['location']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Status :</td><td class='right_td'>".$query_row['status']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Format :</td><td class='right_td'>".$query_row['format']."</td><td></td></tr>";
													echo "<tr><td class='left_td'>Download :</td><td class='right_td'><a href=\"\" onclick=\"window.open('./class/force_dl.php?elif=".$query_row['file']."','download','width=400,height=200,left=0,top=100,screenX=0,screenY=100');\">".$query_row['file']."</a></td><td></td></tr>";
													
												
												echo "</table></div>";
												echo "<div id=\"fullrecord\" class=\"tab_content\"><table width='670px'>";
												//full description
													echo "<tr><td class='left_td'>Tilte :</td><td class='right_td'>".$query_row['title']."</td><td rowspan=\"14\"><img src=\"./e_lib_materials/cover/".$query_row['cover']."\"></td></tr>";
														echo "<tr><td class='left_td'>ISBN/ISSN :</td><td class='right_td'>".$query_row['ISBN_ISSN']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Subject :</td><td class='right_td'>".$query_row['subject']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Author :</td><td class='right_td'>".$query_row['author']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Call Number :</td><td class='right_td'>".$query_row['call_no']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Publisher :</td><td class='right_td'>".$query_row['publisher']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Number of Copies :</td><td class='right_td'>".$query_row['copies']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Physical Description :</td><td class='right_td'>".$query_row['physical_description']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Location :</td><td class='right_td'>".$query_row['location']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Status :</td><td class='right_td'>".$query_row['status']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Format :</td><td class='right_td'>".$query_row['format']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Series Statement :</td><td class='right_td'>".$query_row['series_statement']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>General Note :</td><td class='right_td'>".$query_row['general_note']."</td><td></td></tr>";
														echo "<tr><td class='left_td'>Download :</td><td class='right_td'><a href=\"\" onclick=\"window.open('./class/force_dl.php?elif=".$query_row['file']."','download','width=400,height=200,left=0,top=100,screenX=0,screenY=100');\">".$query_row['file']."</a></td><td></td></tr>";
														
												
												echo "</table></div>";
											
										}
										?>
								</div>
							</div>							
						</div>
			   </div>   				
			</div>
			<div id="footer">
				<?php include '../includes/footer_content.php' ?>		
				</div>
			<div id="floating" style="top:320px;margin-left:-495px;">
				<div id="browntag">
					<img src="../img/decor.png"/>
					<div id="tagLabel">
						E-LIBRARY
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>