<?php ob_start(); ?>
<?php session_start(); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include "../includes/headers2.txt"; include("includes/head.php"); ?>
<?php $place ="search";/*for autocomplete*/ include("script/suggest.php"); //autocomplete--suggest?>
</head>
<body>
<div id="wrapper1">
	<?php include '../includes/headerstyle2.txt'; ?>
			<div id="contents">
				<div id="page-content">
						<div id="dock-icons">
							<?php include '../includes/dockicons2.php'; //icons from the dockbar?>
						</div>
						<div id="contents-holder">							
							<div class="panel-holder" >
								<div id="left">
									<?php include("includes/leftcontent.php"); ?>
								</div>
								<div id="right" class="globalroundedcorners">
									<span class="title" id="search-title">Basic Search</span>									
										<?php include("../includes/connect_db.php");//connect 2 database
										  $categ = mysql_real_escape_string(@$_GET['category']);
										  $categ = stripslashes($categ);//for anti injection
										  $categ = str_replace("'", "", $categ);//for anti injection
										  $srch_arg =  mysql_real_escape_string(@$_GET['searcharg']);
										  
									?>
									<?php function search_basic(){?>
									<div id="upper-search" class="globalroundedcorners">
									Search the POPCOM&#146;s  holding Books,Pamphlets,Reports etc.<br/><br/>
									<form action="" method="GET">
									<select name="category" class="roundedcorners">
									<?php if($categ = str_replace("'", "",stripslashes($_GET['category'])) == "title" ) {?>
										<option value="title" selected="true">Title</option><option value="author">Author</option><option value="subject">Subject</option><option value="call_no">Call Number</option><option value="ISBN_ISSN">ISBN/ISSN</option><option value="keyword">Any Field</option>
									<?php } else if($categ = str_replace("'", "",stripslashes($_GET['category'])) == "author"){ ?>
										<option value="title">Title</option><option value="author" selected="true">Author</option><option value="subject">Subject</option><option value="call_no">Call Number</option><option value="ISBN_ISSN">ISBN/ISSN</option><option value="keyword">Any Field</option>
									<?php } else if($categ = str_replace("'", "",stripslashes($_GET['category'])) == "subject"){ ?>
										<option value="title">Title</option><option value="author">Author</option><option value="subject" selected="true">Subject</option><option value="call_no">Call Number</option><option value="ISBN_ISSN">ISBN/ISSN</option><option value="keyword">Any Field</option>
									<?php } else if($categ = str_replace("'", "",stripslashes($_GET['category'])) == "call_no"){ ?>
										<option value="title">Title</option><option value="author">Author</option><option value="subject">Subject</option><option value="call_no" selected="true">Call Number</option><option value="ISBN_ISSN">ISBN/ISSN</option><option value="keyword">Any Field</option>
									<?php } else if($categ = str_replace("'", "",stripslashes($_GET['category'])) == "ISBN_ISSN"){ ?>
										<option value="title">Title</option><option value="author">Author</option><option value="subject">Subject</option><option value="call_no">Call Number</option><option value="ISBN_ISSN" selected="true">ISBN/ISSN</option><option value="keyword">Any Field</option>
									<?php } else{ ?>
										<option value="title">Title</option><option value="author">Author</option><option value="subject">Subject</option><option value="call_no">Call Number</option><option value="ISBN_ISSN">ISBN/ISSN</option><option value="keyword" selected="true">Any Field</option>
									<?php } ?>
									</select>
									<?php echo "<input maxlength=\"75\" name=\"searcharg\" id=\"searcharg\" size=\"25\" class=\"roundedcorners\" value='".@$_GET['searcharg']."' />" ?>
									<input type="submit" name="doSearch" id="defaultButton" value="Search" class="button orange bigrounded"/>
									</form>
									</div><?php } ?>																
									<?php 
									/********************************************************************/
									require_once("class/clean.php");//for data cleaning
									$clean = new Clean();
									require_once("class/pagination.php");//for pagination
									$paginations = new paginations();
									require_once("class/formelement.php");//for form element
									$formelement = new formelement();
									require_once("includes/Encryption.php");//for encryption
									$secret_code = "imadreamer";//secret code
									$encrypt = new Encryption();
									/********************************************************************/
									if(!isset($_GET['doSearch'])){echo search_basic();}
									else{echo search_basic();
										$srch_arg = trim($srch_arg);
										$srch_arg = str_replace("%","",$srch_arg);
										if(empty($srch_arg)){}
										else{
										$srch_arg = strip_tags($srch_arg);
										if($categ == "keyword"){
												$bs_src = "books WHERE title like '%$srch_arg%' OR subject like '%$srch_arg%' OR author like '%$srch_arg%' OR call_no like '%$srch_arg%' OR ISBN_ISSN like '%$srch_arg%'";
												}
												else{
												$bs_src = "materials WHERE $categ like '%$srch_arg%'";
													}
												
													$connect ="../includes/connect_db.php";
													$table = $bs_src;//table or table with where clause
													$adjacents=3;
													$targetpage="search.php";//target page
													if(isset($_GET['limit'])){$_GET['limit']= $_GET['limit'];}else{$_GET['limit']="";}
													$limit = $clean->RemoveDirt($_GET['limit']);if(empty($limit) || $limit <= 0 || ctype_alpha($limit)){$limit = 10;}else{$limit = $clean->RemoveMagic($limit);}//limit to show in
													if(isset($_GET['page'])){$_GET['page']=$_GET['page'];}else{$_GET['page']="";}
													$page = $_GET['page']; //get page number
													$addtourl = "doSearch=Search&category=".$categ."&searcharg=".$srch_arg."&limit=".$limit;//something you add at the url
													$get_pgntn = $paginations->pagination_query($connect,$table,$adjacents,$targetpage,$limit,$page,$addtourl);
													if($page < 2){$pagenum = 1;}else{$pagenum = (($limit*$page)-1);}//for paging on brief results
													$resultfound = "<center>".$paginations->TotalResult()."<b> Results Found for : ".stripslashes(stripslashes($srch_arg))."</b></center>";
													echo '<div id="pub-contents" class="globalroundedcorners">';
													echo $resultfound;
													echo "<table width='570px' id='pub-table'>";
													echo "<tr><td>".$paginations->show_pagination_pn()."</td><td></td><td></td><td><a href=\"javascript:void(0)\" onclick=\"window.open('includes/print_results.php?print=".$encrypt->encode(addslashes($table).$secret_code)."&fm=1','mywindow','width=550,height=670,scrollbars=yes')\" title=\"Print Results\" style='margin-left:20px;'><img src=\"images/print.gif\" width=\"20\" height=\"20\" ></a></td></tr>";
													echo "<tr><td class='t_head'>Format</td><td class='t_head'>Title</td><td class='t_head'>Location</td><td class='t_head'>Call No.</td></tr>";
													while($query_row =@ mysql_fetch_array($get_pgntn))
													{
														echo "<tr><td class=\"t_result\">".$query_row['format']."</td><td class=\"t_result\"><a href=\"result.php?$addtourl&load=$pagenum\">".$query_row['title']."</a></td><td class=\"t_result\">".$query_row['location']."</td><td class=\"t_result\">".$query_row['call_no']."</td></tr>";
														$pagenum++;
													}
													echo "<tr><td>";
													echo $paginations->show_pagination()."</td>";
													$option = array(2,"2",5,"5",10,"10");
													echo "<td align=\"right\" colspan=3><form action=\"\" method=\"get\">";
													echo "<input type=\"hidden\" name=\"doSearch\" value=\"Search\">";
													echo "<input type=\"hidden\" name=\"category\" value=\"$categ\">";
													echo "<input type=\"hidden\" name=\"searcharg\" value=\"".stripslashes($srch_arg)."\">";
													echo " Records per page".$formelement->OptionSubmit($option,"limit",$limit);
													echo "</form></td></td><td></td><td></tr>";
													echo "</table>";
													echo "</div>";
											}
									}
									?>																	
							</div>							
						</div>
			   </div>        
			</div>
			<div id="footer">
				<?php include '../includes/footer_content.php' ?>		
				</div>
		</div>
	</div>	
</div>
</body>
</html>
<?php ob_flush(); ?>