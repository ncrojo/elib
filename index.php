<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include "../includes/headers2.txt";include("includes/head.php"); ?>
<?php $place ="index";/*for autocomplete*/ include("script/suggest.php"); //autocomplete--suggest?>
</head>
<body>
<div id="wrapper1">
	<div id="fixed-header">
		<?php include '../includes/headerstyle2.txt'; ?>
			<div id="contents">
				<div id="page-content">
						<div id="dock-icons">
							<?php include '../includes/dockicons2.php'; //icons from the dockbar?>
						</div>
						<div id="contents-holder">							
							<div class="panel-holder" >
								<br/><br/><br/><br/>
								<div id="left">
									<?php include("includes/leftcontent.php"); ?>
								</div>
								<div id="right" class="globalroundedcorners">
									<div id="search-holder" class="globalroundedcorners">
										<span class="title" id="basic-search">Basic Search</span><br /><br />								
										<form action="search.php" method="get" id="search-form">
										<select name="category" class="roundedcorners" style="background:white; width:300px;>
										<option value="title">Title</option>
										<option value="author">Author</option>
										<option value="subject">Subject</option>
										<option value="call_no">Call Number</option>
										<option value="ISBN_ISSN">ISBN/ISSN</option>
										<option value="keyword">Any Field</option>
										</select><br/><br />
										<input maxlength="75" name="searcharg" id="searcharg" style="width:285px;height:20px;" value='' class="roundedcorners"/><br/><br />
										<input type="submit" name="doSearch" id="defaultButton" value="Search" class="button orange bigrounded" style="float:right;"/>
										</form>
									</div>
								</div>
							</div>							
						</div>
			   </div>   				
			</div>
			<div id="footer">
				<?php include '../includes/footer_content.php' ?>		
				</div>
			<div id="floating" style="top:320px;margin-left:-495px;">
				<div id="browntag">
					<img src="../img/decor.png"/>
					<div id="tagLabel">
						E-LIBRARY
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>