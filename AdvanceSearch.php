<?php ob_start(); session_start();?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include "../includes/headers2.txt"; include("includes/head.php"); require_once("includes/Encryption.php"); require_once("includes/removeslash.php"); ?>
<?php $place = "advancesearch2"; include("script/suggest.php"); //autocomplete--suggest?>
</head>
<body>
<div id="wrapper1">
<div id="fixed-header">
		<?php include '../includes/headerstyle2.txt'; ?>
			<div id="contents">
				<div id="page-content">
						<div id="dock-icons">
							<?php include '../includes/dockicons2.php'; //icons from the dockbar?>
						</div>
						<div id="contents-holder">							
							<div class="panel-holder" >
								<div id="left">
									<?php include("includes/leftcontent.php"); ?>
								</div>
								<div id="right" class="globalroundedcorners">
									<span class="title" id="search-title">Advance Search</span><br /><br />
											<form action="" method="get" id="adv-form" class="globalroundedcorners">
											<ul class="advsearch-style">
												<li><span class="search-header">Category</span></li>
												<li><span class="search-header">Search Argument</span></li>
											</ul>
											<ul class="advsearch-style">
												<li><select name="category[]" class="roundedcorners"><option value="title">Title</option><option value="author">Author</option><option value="subject">Subject</option><option value="call_no">Call Number</option><option value="ISBN_ISSN">ISBN/ISSN</option><option value="keyword">Any Field</option></select></li>
												<li><input name="searcharg[]" size="25"  id="searcharg2" class="roundedcorners"/></li>
												<li><select name="option[]" class="roundedcorners"><option value="OR">OR</option><option value="AND">AND</option></select></li>
											</ul>
											<ul class="advsearch-style">
												<li><select name="category[]" class="roundedcorners"><option value="title">Title</option><option value="author">Author</option><option value="subject">Subject</option><option value="call_no">Call Number</option><option value="ISBN_ISSN">ISBN/ISSN</option><option value="keyword">Any Field</option></select></li>
												<li><input name="searcharg[]" size="25" id="searcharg1" class="roundedcorners"/></li>
												<li><select name="option[]" class="roundedcorners"><option value="OR">OR</option><option value="AND">AND</option></select></li>
											</ul>
											<div id="format-holder" class="globalroundedcorners">
												<h2><span style="font-weight:bold; color:#003f0d; padding:10px 155px 10px 155px; background:#f3c800;border-bottom-left-radius:10px;border-bottom-right-radius:10px;">Format</span></h2><br/>
												<ul class="checkbox-holder">
													<li>
														<div class="divider-holder">
															<ul>
																<li><input type="Checkbox" name="format[]" value="Book">&nbsp;&nbsp;&nbsp;Book</li>
																<li><input type="Checkbox" name="format[]" value="Dissertation">&nbsp;&nbsp;&nbsp;Dissertation</li>
																<li><input type="Checkbox" name="format[]" value="Flip Chart">&nbsp;&nbsp;&nbsp;Flip Chart</li>												
																<li><input type="Checkbox" name="format[]" value="Flyer">&nbsp;&nbsp;&nbsp;Flyer</li>
																<li><input type="Checkbox" name="format[]" value="Pamphlet">&nbsp;&nbsp;&nbsp;Pamphlet</li>
																<li><input type="Checkbox" name="format[]" value="Periodical">&nbsp;&nbsp;&nbsp;Periodical</li>
															</ul>
														</div>
													</li>
													<li>
														<div class="divider-holder">
															<ul>
																<li><input type="Checkbox" name="format[]" value="Poster">&nbsp;&nbsp;&nbsp;Poster</li> 
																<li><input type="Checkbox" name="format[]" value="Thesis">&nbsp;&nbsp;&nbsp;Thesis</li> 
																<li><input type="Checkbox" name="format[]" value="Vertical File">&nbsp;&nbsp;&nbsp;Vertical File</li>
																<li><input type="Checkbox" name="format[]" value="VHS">&nbsp;&nbsp;&nbsp;VHS</li>
																<li><input type="Checkbox" name="format[]" value="Comics">&nbsp;&nbsp;&nbsp;Comics</li> 
															</ul>
														</div>
													</li>
													<li>
														<div class="divider-holder">
															<ul>
																<li><input type="Checkbox" name="format[]" value="CD-ROM">&nbsp;&nbsp;&nbsp;CD-ROM</li>
																<li><input type="Checkbox" name="format[]" value="DVD">&nbsp;&nbsp;&nbsp;DVD</li>
																<li><input type="Checkbox" name="format[]" value="Audio">&nbsp;&nbsp;&nbsp;Audio</li>
																<li><input type="Checkbox" name="format[]" value="Video">&nbsp;&nbsp;&nbsp;Video<li>
															</ul>
														</div>
													</li>
												</ul>
											</div>																						
											<input type="submit" name="AddoSearch" id="defaultButton" value="Search" class="button orange bigrounded" style="float:right; margin-top:20px;"/>
											</form>
											<?php 
											if(isset($_GET['AddoSearch']))
											{	
												include("../includes/connect_db.php");
												$category = $_GET['category'];//category
												$searcharg = $_GET['searcharg'];//search argument
												$option = $_GET['option'];//option
												/*CONTSRACT the QUERY*/
												$advnce_query = "(";//advance query
												$rptr = 0;
												while($rptr < 2)
												{	//secure data
													$category[$rptr] = removeslash($category[$rptr]);//remove slashes
													$category[$rptr] = str_replace("\"","",$category[$rptr]);//remove "
													$category[$rptr] = str_replace("/","",$category[$rptr]);//remove /
													$category[$rptr] = str_replace("'","",$category[$rptr]);//remove s'
													if(empty($category[$rptr])){$category[$rptr] = "title";}//validates if empty
													//end secure data
													$advnce_query .= $category[$rptr]." LIKE \"%".trim(mysql_real_escape_string(str_replace("%","",$searcharg[$rptr])))."%\" ";
													//secure data
													$option[$rptr] = removeslash($option[$rptr]);//remove slashes
													$option[$rptr] = str_replace("\"","",$option[$rptr]);//remove "
													$option[$rptr] = str_replace("/","",$option[$rptr]);//remove /
													$option[$rptr] = str_replace("'","",$option[$rptr]);//remove '
													if(empty($option[$rptr])){$option[$rptr] = "";}//validates if empty
													//end secure data
													if($rptr == 1){$advnce_query .=")".$option[$rptr]." ";$remove = strlen($option[$rptr]);/*count no. of character to remove*/ }
													else{$advnce_query .=$option[$rptr]." ";}
													$rptr++;
												}	
												$format = $_GET['format'];//format
												if(empty($format)){$advnce_query = substr_replace($advnce_query,"",-($remove+1));}//remove option if there is no format selected
												$cnt = count($_GET['format']);//format
												$repeater = 0;
												if($cnt >=1)
												{
													while($repeater < $cnt)
													{	//secure data
														$format[$repeater] = removeslash($format[$repeater]);//remove slashes
														$format[$repeater] = str_replace("\"","",$format[$repeater]);//remove "
														$format[$repeater] = str_replace("/","",$format[$repeater]);//remove /
														$format[$repeater] = str_replace("'","",$format[$repeater]);//remove '
														if(empty($format[$repeater])){$format[$repeater] = "Book";}//validates if empty
														//end secure data
														$advnce_query .=" format =\"".$format[$repeater]."\"";	
														if($repeater != ($cnt-1)){$advnce_query .=" OR ";}
														else{$advnce_query .="";}
														$repeater++;
													}
												}
												else{}/*--end of constrating query*/
												if(!empty($searcharg[0])||!empty($searcharg[1])||($cnt >=1))//start searching
												{	$secret_code = "imadreamer";
													$advnce_query = $advnce_query.$secret_code;//addsecret code
													$encrypt_query = new Encryption();
													$encrypted_query = $encrypt_query->encode($advnce_query);//encrypt query
													echo $advnce_query;
													header("Location: SearchAdvance.php?qid=".$encrypted_query);//redirect to query page
												}
												else{echo "<br /><br />";}//if there is nothing to search for
											}
											?>
									</div>
								</div>
							</div>							
						</div>
			   </div> 
				<div id="footer">
				<?php include '../includes/footer_content.php' ?>		
				</div>
			</div>
		</div>	
</div>
</body>
</html>
<?php ob_flush(); ?>