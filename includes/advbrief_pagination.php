<?php
	/*
		Place code to connect to your DB here.
	*/
	include("../includes/connect_db.php");	// include your code to connect to DB.
	$select=$brief;		//your table name
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	$query = "SELECT COUNT(*) as num FROM $select";
	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages['num'];	
	/* Setup vars for query. */
	$targetpage = "advanceresult.php"; 	//your file name  (the name of this file)
	$limit = 1; 								//how many items to show per page
	$page = mysql_real_escape_string($_GET['load']);
	
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0	
	/* Get data. */
	$sql = "SELECT * FROM $select LIMIT $start, $limit";
	$result = mysql_query($sql);	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";$counter="";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage?qid=$qid&load=$prev\">&#171; previous</a>";
		else
			$pagination.= "<span class=\"disabled\">&#171; previous</span>";	
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a href=\"$targetpage?qid=$qid&load=$counter\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?qid=$qid&load=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?qid=$qid&load=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?qid=$qid&load=$lastpage\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?qid=$qid&load=1\">1</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?qid=$qid&load=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?qid=$qid&load=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?qid=$qid&load=$lastpage\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage?qid=$qid&load=1\">1</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?qid=$qid&load=$counter\">$counter</a>";					
				}
			}
		}
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"$targetpage?qid=$qid&load=$next\">next &#187;</a>";
		else
			$pagination.= "<span class=\"disabled\">next &#187;</span>";
		$pagination.= "</div>\n";		
	}
	//top pagination
	$top_pagntion ="";	
	//preview button			
	if ($page > 1) {
	$top_pagntion.= "<a href=\"$targetpage?qid=$qid&load=$prev\">&#171; previous</a>&nbsp;&nbsp;&nbsp;";
	}
	else{
	$top_pagntion.= "<span class=\"disabled\">&#171; previous</span>&nbsp;&nbsp;&nbsp;";						
	}
	//next button
	if ($page < $counter - 1) 
		$top_pagntion.= "<a href=\"$targetpage?qid=$qid&load=$next\">next &#187;</a>";
	else
		$top_pagntion.= "<span class=\"disabled\">next &#187;</span>";
	
	echo  $top_pagntion;
?>
	<?php	
			//--show results
			$brief_row =@mysql_fetch_array($result);
			echo "<div id=\"briefrecord\" class=\"tab_content\">";
			echo "<table width='800px'>";
			
			
			echo "<tr><td class='left_td'>Tilte :</td><td class='right_td'>".$brief_row['title']."</td><td rowspan=\"7\"><img src=\"./e_lib_materials/cover/".$brief_row['cover']."\"></td></tr>";
			echo "<tr><td class='left_td'>Subject :</td><td class='right_td'>".$brief_row['subject']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Physical Description :</td><td class='right_td'>".$brief_row['physical_description']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Location :</td><td class='right_td'>".$brief_row['location']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Status :</td><td class='right_td'>".$brief_row['status']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Format :</td><td class='right_td'>".$brief_row['format']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Download :</td><td class='right_td'><a href=\"\" onclick=\"window.open('./class/force_dl.php?elif=".$brief_row['file']."','download','width=400,height=200,left=0,top=100,screenX=0,screenY=100');\">".$brief_row['file']."</a></td><td></td></tr>";
			
			
			echo "</table></div>";
			//////////////
			echo "<div id=\"fullrecord\" class=\"tab_content\">";
			echo "<table width='800px'>";
			
			echo "<tr><td class='left_td'>Tilte :</td><td class='right_td'>".$brief_row['title']."</td><td rowspan=\"14\"><img src=\"./e_lib_materials/cover/".$brief_row['cover']."\"></td></tr>";
			echo "<tr><td class='left_td'>ISBN/ISSN :</td><td class='right_td'>".$brief_row['ISBN_ISSN']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Subject :</td><td class='right_td'>".$brief_row['subject']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Author :</td><td class='right_td'>".$brief_row['author']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Call Number :</td><td class='right_td'>".$brief_row['call_no']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Publisher :</td><td class='right_td'>".$brief_row['publisher']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Number of Copies :</td><td class='right_td'>".$brief_row['copies']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Physical Description :</td><td class='right_td'>".$brief_row['physical_description']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Location :</td><td class='right_td'>".$brief_row['location']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Status :</td><td class='right_td'>".$brief_row['status']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Format :</td><td class='right_td'>".$brief_row['format']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Series Statement :</td><td class='right_td'>".$brief_row['series_statement']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>General Note :</td><td class='right_td'>".$brief_row['general_note']."</td><td></td></tr>";
			echo "<tr><td class='left_td'>Download :</td><td class='right_td'><a href=\"\" onclick=\"window.open('./class/force_dl.php?elif=".$brief_row['file']."','download','width=400,height=200,left=0,top=100,screenX=0,screenY=100');\">".$brief_row['file']."</a></td><td></td></tr>";
			
			
			echo "</table></div>";
			
	?>
<?php 

 ?>
	