<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include("includes/head.php"); ?>
<script type="text/javascript">
/*js for the tab*/
$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
</script>
</head>
<body>
<div id="wrapper">
<div id="header">
	<div id="search">
	<!--search form-->
	</div>
	<div id="main_nav">
		<?php include("includes/mainnav.php"); ?>
	</div>
</div>
<div id="nav">
<!--internal links or sublinks-->

</div>
<div  id="contents">
	<div id="left">
		<?php include("includes/leftcontent.php"); ?>
	</div>
	<div id="right">
		<?php
		 include("../includes/connect_db.php");

			$categ =@ mysql_real_escape_string($_GET['category']); 
			$srch_arg =@ mysql_real_escape_string($_GET['searcharg']); 
			$categ = stripslashes($categ);
			$categ = str_replace("'", "", $categ);
			$categ = str_replace("\\", "", $categ);

		?>
		<ul class="tabs">
		<li><a href="#briefrecord">Brief Record</a></li>
		<li><a href="#fullrecord">Full Record</a></li>
		&nbsp;&nbsp;&nbsp;<a href="search.php?category=<?php echo stripslashes($categ); ?>&searcharg=<?php echo stripslashes($srch_arg); ?>&doSearch=Search&limit=<?php echo $_GET['limit']; ?>">Back to Search Result</a>
		</ul>

		<div class="tab_container">
			<?php include("../includes/connect_db.php");
			
			if($categ == "keyword"){$brief = "materials WHERE title like \"%$srch_arg%\" OR subject like \"%$srch_arg%\" OR author like \"%$srch_arg%\" OR call_no like \"%$srch_arg%\" OR ISBN_ISSN like \"%$srch_arg%\"";}
			else{$brief = "materials WHERE $categ like \"%$srch_arg%\"";}
			$reslt_reslt =@ mysql_query("SELECT * FROM $brief "); ?>
			<?php 
				include("includes/brief_pagination.php");
			?>

		</div>
	
	</div>
	<div class="clear"></div>
</div>

</div>
<div id="footer">
<?php include("includes/footer.php"); ?>
</div>
</body>
</html>