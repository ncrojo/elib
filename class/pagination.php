<?php

class paginations{
	//include("connect_db.php");	// include your code to connect to DB.
	var $pagination;
	var $pagination_pn;
	var $total_result;
	function pagination_query($connect,$table,$adjacents,$targetpage,$limit,$page,$addtourl)
	{	
		if(!empty($addtourl)){$addtourl = $addtourl."&";}else{$addtourl = $addtourl;}
		include($connect);	// include your code to connect to DB.
		$select=$table;		//your table /table with WHERE
		// How many adjacent pages should be shown on each side?
		$adjacents = $adjacents;
		/* 
		   First get total number of rows in data table. 
		   If you have a WHERE clause in your query, make sure you mirror it here.
		*/
		$query = "SELECT COUNT(*) as num FROM $select";
		$total_pages =@ mysql_fetch_array(mysql_query($query));
		$total_pages = $total_pages['num'];	
		$this->total_result = $total_pages['num'];	//total numberofresults
		/* Setup vars for query. */
		$targetpage = $targetpage; 	//your file name  (the name of this file)
		$limit = $limit; 								//how many items to show per page
		$page = $page;
		if($page)
			$start = ($page - 1) * $limit;			//first item to display on this page
		else
			$start = 0;								//if no page var is given, set start to 0	
		/* Get data. */
		$sql = "SELECT * FROM $select LIMIT $start, $limit";
		$result =@ mysql_query($sql);	
		/* Setup page vars for display. */
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
		/* 
			Now we apply our rules and draw the pagination object. 
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		$this->pagination = "";
		if($lastpage > 1)
		{	
			//previous button
			if ($page > 1) {
				$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$prev\">&#171; previous</a>";
				$this->pagination_pn.= "&nbsp;&nbsp;<a class=\"off\" href=\"$targetpage?$addtourl"."page=$prev\">&#171; previous</a>";
				}
			else{
				$this->pagination.= "<a class=\"on\">&#171; previous</a>";	
				$this->pagination_pn.= "&nbsp;&nbsp;<a class=\"on\">&#171; previous</a>";	
				}
			//pages	
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$this->pagination.= "<a class=\"on\">$counter</a>";
					else
						$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$counter\">$counter</a>";					
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2))		
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$this->pagination.= "<a class=\"on\">$counter</a>";
						else
							$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$counter\">$counter</a>";					
					}
					$this->pagination.= "...";
					$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$lpm1\">$lpm1</a>";
					$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$lastpage\">$lastpage</a>";		
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=1\">1</a>";
					$this->pagination.= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"on\">$counter</span>";
						else
							$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$counter\">$counter</a>";					
					}
					$this->pagination.= "...";
					$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$lpm1\">$lpm1</a>";
					$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$lastpage\">$lastpage</a>";		
				}
				//close to end; only hide early pages
				else
				{
					$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=1\">1</a>";
					$this->pagination.= "...";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$this->pagination.= "<a class=\"on\">$counter</a>";
						else
							$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$counter\">$counter</a>";					
					}
				}
			}
			//next button
			if ($page < $counter - 1) {
				$this->pagination.= "<a class=\"off\" href=\"$targetpage?$addtourl"."page=$next\">next &#187;</a>";
				$this->pagination_pn.= "&nbsp;&nbsp;<a class=\"off\" href=\"$targetpage?$addtourl"."page=$next\">next &#187;</a>";
				}
			else{
				$this->pagination.= "<a class=\"on\">next &#187;</a>";
				$this->pagination_pn.= "&nbsp;&nbsp;<a class=\"on\">next &#187;</a>";
				}

		}

		//for paging on results
				if($page < 2){$pagenum = 1;}else{$pagenum = (($limit*$page)-1);}
				
				//$bsc_reslt = mysql_fetch_array($result);
				return $result;
		
	}
	function show_pagination(){
	return $this->pagination; //full pagination 
	}
	function show_pagination_pn(){
	return $this->pagination_pn; //preview and next only
	}
	function TotalResult(){
	return $this->total_result; //total number of rows 
	}

}?>
	