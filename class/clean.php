<?php
class Clean{
	function RemoveDirt($dirty)
	{	
		if(get_magic_quotes_gpc()==1)
		{
			$clean = $dirty;
			return $clean;
		}
		else
		{
			$clean = addslashes($dirty);
			return $clean;
		}
	}
	function RemoveMagic($dirty)
	{
		
		$clean = preg_replace('#\W#', '',$dirty);//remove everything execpt numbers and letters
		
		return $clean;
	}
	function RemoveBands($dirty)
	{
		$badchars = ARRAY (";","%", "'", "\"", "*", "DROP", "SELECT", "UPDATE", "DELETE", "-");
		$clean = str_replace($badchars, "",$dirty);//remove badwords and symbols
		
		return $clean;
	}
}
?>