<?php

function force_download($directory,$file)
{			//  "../log/exports/"
    $dir      = $directory;
    $files      = $dir.$file;//full path
    if ((isset($file))&&(file_exists($files))) {
       
		// Parse Info / Get Extension
		$fsize = filesize($files);
		$path_parts = pathinfo($files);
		$ext = strtolower($path_parts["extension"]);
	   
		// Determine Content Type
		switch ($ext) {
		  case "pdf": $ctype="application/pdf"; break;
		  case "exe": $ctype="application/octet-stream"; break;
		  case "zip": $ctype="application/zip"; break;
		  case "doc": $ctype="application/msword"; break;
		  case "xls": $ctype="application/vnd.ms-excel"; break;
		  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
		  case "gif": $ctype="image/gif"; break;
		  case "png": $ctype="image/png"; break;
		  case "jpeg":
		  case "jpg": $ctype="image/jpg"; break;
		  default: $ctype="application/force-download";
		} 
	   
	   header("Content-type: ".$ctype);
	   
       header('Content-Disposition: inline; filename="' . $files . '"');
       header("Content-Transfer-Encoding: Binary");
       header("Content-length: ".filesize($files));
       header('Content-Type: application/octet-stream');
       header('Content-Disposition: attachment; filename="' . $file . '"');
       readfile("$files");
    } else {
       echo "No file selected";
    } //end if

}


$file = @$_GET['elif'];
echo force_download("../e_lib_materials/",$file);

?>